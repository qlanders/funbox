"use strict";

const gulp         = require('gulp'),
      browserSync  = require('browser-sync'),
      reload       = browserSync.reload,
      del          = require('del'),
      pug          = require('gulp-pug'),
      cache        = require('gulp-cache'),
      notify       = require('gulp-notify'),
      imgMin       = require('gulp-imagemin'),
      pngquant     = require('imagemin-pngquant'),
      pugData      = require('./app/data/pug_data.json'),
      postcss      = require('gulp-postcss'),
      autoprefixer = require('autoprefixer'),
      assets       = require('postcss-assets'),
      sass         = require('gulp-sass'),
      comments     = require('postcss-discard-comments'),
      mqpack       = require('css-mqpacker'),
      csso         = require('gulp-csso'),
      sourcemaps   = require('gulp-sourcemaps'),
      runSequence  = require('run-sequence'),
      htmlreplace  = require('gulp-html-replace'),
      rename       = require('gulp-rename'),
      concat       = require('gulp-concat');


let PATH = {
	SRC         : './app',
	SRC_STYLES  : './app/css',
	SRC_SCRIPTS : './app/js',
	SRC_IMG     : './app/images',
	SRC_FONTS   : './app/fonts',
	SRC_LIBS    : './app/libs',
	DIST        : './dist',
	DIST_STYLES : './dist/css',
	DIST_SCRIPTS: './dist/js',
	DIST_IMG    : './dist/images',
	DIST_FONTS  : './dist/fonts'
};

//---------------------------------------SYNC

gulp.task('sync', () => {
	browserSync.init({
		server: {
			baseDir: PATH.SRC
		},
		notify: false
	});
});

//---------------------------------------PUG

gulp.task('pug', () => {
	return gulp.src(PATH.SRC + '/*.pug')
		.pipe(pug({
			locals: pugData
		}))
		.on('error', notify.onError({
			message: "<%= error.message %>",
			title  : "Pug error!"
		}))
		.pipe(gulp.dest(PATH.SRC));
});

//---------------------------------------CSS

gulp.task('css', () => {
	let processors = [
		assets({
			basePath   : './app/',
			loadPaths  : ['./images/', './fonts/'],
			baseUrl    : '/',
			cachebuster: true,
			relative   : true
		})
	];
	return gulp.src([PATH.SRC + '/sass/**/*'], {
		dot: true
	})
		.pipe(sourcemaps.init())
		.pipe(sass())
		.on('error', notify.onError({
			message: "<%= error.message %>",
			title  : "sass error!"
		}))
		.pipe(postcss(processors))
		.on('error', notify.onError({
			message: "<%= error.message %>",
			title  : "postcss error!"
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(PATH.SRC + '/css/'))
		.pipe(reload({stream: true}));
});

gulp.task('cssBuild', () => {
	let processors = [
		comments({
			removeAll: true
		}),
		assets({
			basePath   : './app/',
			loadPaths  : ['./images/', './fonts/'],
			//baseUrl    : '/local/',
			cachebuster: true,
			relative   : true
		}),
		mqpack({
			sort: true
		}),
		autoprefixer({
			browsers: ['last 2 versions', 'not ie <= 10']
		})
	];
	return gulp.src([PATH.SRC + '/sass/**/*'], {
		dot: true
	})
		.pipe(sass())
		.pipe(postcss(processors))
		.pipe(csso())
		.pipe(rename('styles.min.css'))
		.pipe(gulp.dest(PATH.DIST_STYLES));
});

//---------------------------------------IMAGES

gulp.task('img', () => {
	return gulp.src([
		PATH.SRC_IMG + '/**/*'])
		.pipe(cache(imgMin({
			intarlaced : true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			une        : [pngquant()]
		})))
		.pipe(gulp.dest(PATH.DIST_IMG));
});


//---------------------------------------CLEAN

gulp.task('clean', () => {
	return del.sync(PATH.DIST);
});

//---------------------------------------WATCH

gulp.task('default', () => {
	runSequence(
		'sync',
		['css', 'pug']
	);
	gulp.watch('./app/sass/**/*.sass', ['css']);
	gulp.watch('./app/**/*.pug', ['pug']);
	gulp.watch('./app/index.html', reload);
});

//---------------------------------------BUILD

gulp.task('build', function () {

	runSequence(
		'clean',
		['img', 'pug', 'cssBuild']
	);

	gulp.src(PATH.SRC_FONTS + '/**/*')
		.pipe(gulp.dest(PATH.DIST_FONTS));

	gulp.src([PATH.SRC + '/**/*.html'])
		.pipe(htmlreplace({
			'css': 'css/styles.min.css',
			'js' : 'js/bundle.min.js'
		}))
		.pipe(gulp.dest(PATH.DIST));

	gulp.src(PATH.SRC_IMG + '/sprite.svg')
		.pipe(gulp.dest(PATH.DIST_IMG));

});
